FROM alpine:latest

LABEL maintainer=hgadgil@picompute.io

RUN apk update && apk upgrade
RUN apk add bash jq curl

COPY register.sh /

ENV API_KEY=
ENV ZONE_ID=
ENV DNS_RECORD_NAME=
ENV EMAIL=

ENTRYPOINT [ "/bin/bash", "/register.sh" ]
