# cloudflare-ddns
Programatically update an DNS `A` record in Cloudflare

# Obtain DNS ZoneId and Global API Key from Cloudflare Dashboard

To get your Cloudflare Zone ID and API key, log in to the Cloudflare Dashboard for the domain you want to use.

* `ZONE_ID`:
 Your Zone ID will be displayed in the bottom right corner.
 
![Zone Id](https://gitlab.com/picompute-lab/cloudflare-ddns/-/raw/main/zone_id.png)

* `API_KEY`:
To get your API Key, click Get your API token just below your Zone ID. Click API Tokens on the gray menu bar at the top, and then look for Global API Key and click the View button

![API Token](https://gitlab.com/picompute-lab/cloudflare-ddns/-/raw/main/api_token.png)

* `DNS_RECORD_NAME`:
Create a dummy A record with value 1.2.3.4, this will be the DNS_RECORD_NAME to update

* `EMAIL`:
Email is the sign-in email for the Cloudflare account

# Running the dockerized script

1. Generate docker image
```
docker build . -t cf-ddns
```

2. Run
```
docker run --rm \
-e API_KEY=<INSERT_VALUE> \
-e ZONE_ID=<INSERT_VALUE> \
-e DNS_RECORD_NAME=<INSERT_VALUE> \
-e EMAIL=<INSERT_VALUE> \
cf-ddns
```
