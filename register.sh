#!/usr/bin/env bash

if [ -z "$API_KEY" ]; then
  echo "API_KEY not set"
  exit -1;
fi
if [ -z "$ZONE_ID" ]; then
  echo "ZONE_ID not set"
  exit -1;
fi
if [ -z "$DNS_RECORD_NAME" ]; then
  echo "DNS_RECORD_NAME not set"
  exit -1;
fi
if [ -z "$EMAIL" ]; then
  echo "EMAIL not set"
  exit -1;
fi

# Get the DNS Record Id for the DDNS Record
DNS_RECORD_ID=`curl -s -X GET "https://api.cloudflare.com/client/v4/zones/$ZONE_ID/dns_records?name=$DNS_RECORD_NAME" \
-H "Content-Type: application/json" \
-H "X-Auth-Email: $EMAIL" \
-H "X-Auth-Key: $API_KEY" | jq -r '.result[0].id'`

# Get my public IP address
MY_IP=`curl -s https://api.ipify.org/\?format\=json | jq -r .ip`

# Generate payload for updating DDNS
echo "{\"id\":\"$ZONE_ID\", \"type\":\"A\", \"name\":\"$DNS_RECORD_NAME\", \"content\":\"$MY_IP\" }" | jq  > data.json

# Update
RESULT=`curl -s -X PUT "https://api.cloudflare.com/client/v4/zones/$ZONE_ID/dns_records/$DNS_RECORD_ID" \
-H "Content-Type: application/json" \
-H "X-Auth-Email: $EMAIL" \
-H "X-Auth-Key: $API_KEY" \
--data @data.json`

echo $RESULT | jq

SUCCESS=`echo $RESULT | jq -r '.success'`

if [ "$SUCCESS" == "true" ]; then
  echo "Successfully updated DDNS Record"
else
  echo "Failed to update DDNS record"
fi
